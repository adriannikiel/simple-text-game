# Escape from Office

This is simple text game: "Escape from Office"

## Description

Welcome to Escape Office, a thrilling game where you are on a mission to find secret documents from your boss and escape the office before getting caught.

As you navigate through the various locations of the office, you'll need to search for clues, solve puzzles, and unlock doors to progress further. However, the game doesn't have a timer or clock ticking down, allowing you to take your time and fully immerse yourself in the game's environment without any added pressure. But don't get too comfortable - the office is full of challenges and obstacles that will test your skills and abilities to think creatively and logically. Can you successfully find the secret documents and make your escape before it's too late? 

Get ready to find out in Escape Office!

![image](https://gitlab.com/adriannikiel/simple-text-game/-/raw/master/SimpleTextGame/assets/TextGameMap.png)
## Short video

https://youtu.be/4liqdyVpEQw
