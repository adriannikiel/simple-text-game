﻿using System;

namespace SimpleTextGame.items
{
    public class PhoneItem : Item, IUsable
    {
        public PhoneItem() : base("PHONE", true, false)
        {
        }

        public void Use()
        {
            Console.WriteLine("You are reading some information about your BOSS. He has publiced some photos of his children. His youngest child was born in 2021.");
        }
    }
}
