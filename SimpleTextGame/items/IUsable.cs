﻿namespace SimpleTextGame
{
    public interface IUsable
    {
        void Use();
    }
}