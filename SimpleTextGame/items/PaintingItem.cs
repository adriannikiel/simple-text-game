﻿using System;

namespace SimpleTextGame.items
{
    public class PaintingItem : Item, IUsable
    {
        private bool isMoved = false;
        private Location location;

        public PaintingItem(Location location) : base("PAINTING", true, false)
        {
            this.location = location;
        }

        public void Use()
        {
            if (!isMoved)
            {
                Console.WriteLine("You have just moved the painting. There is something behind ...  Oh there are documents!");

                Item documents = new Item("DOCUMENTS", false, true);
                documents.Description = $"{documents.ItemName} - There are documents you were looking for.";
                location.AddItem(documents);

                isMoved = true;
            }
            else
            {
                Console.WriteLine("The painting is moved.");
            }
        }
    }
}
