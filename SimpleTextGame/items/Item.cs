﻿using System;

namespace SimpleTextGame
{
    public class Item
    {
        public string ItemName { get; private set; }
        public string Description { get; set; }
        public bool IsUsable { get; private set; }
        public bool IsCollectible { get; private set; }

        public Item(string name, bool isUsable, bool isCollectible)
        {
            ItemName = name;
            IsUsable = isUsable;
            IsCollectible = isCollectible;
        }

        public void DescribeItem()
        {
            Console.WriteLine(Description);
        }

        public override bool Equals(object obj)
        {
            return obj is Item other && this.ItemName == other.ItemName;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ItemName);
        }
    }
}
