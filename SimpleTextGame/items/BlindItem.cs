﻿using System;

namespace SimpleTextGame.items
{
    public class BlindItem : Item, IUsable
    {
        private Room room;

        public BlindItem(Room room) : base("BLIND", true, false)
        {
            this.room = room;
        }

        public void Use()
        {
            if (!room.IsDarkned)
            {
                Console.WriteLine("You just used a roller blind. The room is now darkened. ");
                room.IsDarkned = true;
            }
            else
            {
                Console.WriteLine("You just used a roller blind. Now the room is bright. ");
                room.IsDarkned = false;
            }
        }
    }
}
