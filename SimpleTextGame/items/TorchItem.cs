﻿using System;

namespace SimpleTextGame.items
{
    public class TorchItem : Item, IUsable
    {
        private Room room;
        public bool isON { get; private set; } = false;

        public TorchItem(Room room) : base("TORCH", true, true)
        {
            this.room = room;
        }

        public void Use()
        {
            if (!isON)
            {
                isON = true;
                Console.WriteLine("The torch is ON now. Now you can see in the darkness.");

                Item picture = room.GetLocations()[4].AvailableItems[1];

                if (room.IsDarkned)
                {
                    picture.Description = $"{picture.ItemName} - it is a big picture of your BOSS. Mhhh you see some fingerprints on it.";
                }
                else
                {
                    picture.Description = $"{picture.ItemName} - it is a big picture of your BOSS.";
                }
            }
            else
            {
                isON = false;
                Console.WriteLine("The torch is OFF now.");

                Item picture = room.GetLocations()[4].AvailableItems[1];
                picture.Description = $"{picture.ItemName} - it is a big picture of your BOSS.";
            }
        }
    }
}
