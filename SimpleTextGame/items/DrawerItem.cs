﻿using System;

namespace SimpleTextGame.items
{
    public class DrawerItem : Item, IUsable
    {
        private bool isMoved = false;
        private Location location;

        public DrawerItem(Location location) : base("DRAWER", true, false)
        {
            this.location = location;
        }

        public void Use()
        {
            Description = $"{this.ItemName} - Is opened.";

            if (!isMoved)
            {
                Console.WriteLine("You have just opened the drawer. There is torch inside. Maybe you should take it?");

                Item torch = new TorchItem(location.Room);
                torch.Description = $"{torch.ItemName} - It is useful when the room is dark.";
                location.AddItem(torch);

                isMoved = true;
            }
            else
            {
                Console.WriteLine("The drawer is opened.");
            }
        }
    }
}
