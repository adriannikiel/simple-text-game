﻿using System;

namespace SimpleTextGame
{
    public class HandleItem : Item, IUsable
    {
        public Location LocationToUnlock { get; set; }

        public HandleItem() : base("HANDLE", true, false)
        {
        }

        public void Use()
        {
            Console.WriteLine("You unlocked the DOOR.");
            LocationToUnlock.IsLocked = false;
        }
    }
}
