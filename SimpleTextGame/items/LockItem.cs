﻿using System;

namespace SimpleTextGame.items
{
    public class LockItem : Item, IUsable
    {
        public Location LocationToUnlock { get; set; }

        public LockItem() : base("LOCK", true, false)
        {
        }

        public void Use()
        {
            Console.Write("Please write PIN code: ");

            if (Console.ReadLine() == "2021")
            {
                Console.WriteLine("You heard some fancy sound. The DOOR is unlocked!");
                LocationToUnlock.IsLocked = false;
            }
            else
            {
                Console.WriteLine("Wrong PIN!");
            }
        }
    }
}
