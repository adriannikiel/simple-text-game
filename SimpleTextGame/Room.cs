﻿using System;
using System.Collections.Generic;

namespace SimpleTextGame
{
    public class Room
    {
        public string RoomName { get; private set; }
        public Room AdjacentRoom { get; set; }
        private Dictionary<int, Location> locations = new Dictionary<int, Location>();
        public bool IsDarkned { get; set; } = false;

        public Room(string name)
        {
            RoomName = name;
        }

        public void AddLocation(int locationID, Location location)
        {
            locations.Add(locationID, location);
        }

        public Dictionary<int, Location> GetLocations()
        {
            return new Dictionary<int, Location>(locations);
        }

        public void DescribeRoom()
        {
            Console.WriteLine($"\nYou are in a {RoomName.ToUpper()}.");

            if (IsDarkned)
            {
                Console.WriteLine($"The room is dark.");
            }
        }
    }
}
