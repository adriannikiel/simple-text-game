﻿using System;
using System.Collections.Generic;

namespace SimpleTextGame
{
    public class Game
    {
        public string Name { get; private set; }
        private Dictionary<string, string> vocabulary = new Dictionary<string, string>();
        public List<Item> collectedItems = new List<Item>();
        private Location currentLocation;

        public Game(string name)
        {
            Name = name;
            initGame();
        }

        public void Play()
        {
            ShowIntro();
            Console.Write("-----------------------------------");
            describeLocation();

            while (true)
            {
                Console.WriteLine("-----------------------------------");
                Console.Write("What do you want to do?: ");

                String input = getInput();

                switch (input)
                {
                    case "GO":
                        changeLocation();
                        break;
                    case "USE":
                        useItem();
                        break;
                    case "TAKE":
                        takeItem();
                        break;
                    case "ITEMS":
                        showCollectedItems();
                        break;
                    case "DESCRIBE":
                        describeLocation();
                        break;
                    case "HELP":
                        showHelp();
                        break;
                    case "EXIT":
                        exitGame();
                        return;
                    default:
                        Console.WriteLine("\nWrong command.");
                        showHelp();
                        break;
                }
            }
        }

        private string getInput()
        {
            String input = Console.ReadLine().ToUpper();

            if (vocabulary.ContainsKey(input))
            {
                input = vocabulary[input];
            }

            return input;
        }

        private void initGame()
        {
            Room office = new Room("OFFICE");
            office.AddLocation(0, new WindowLocation(office));
            office.AddLocation(1, new DeskLocation(office));
            office.AddLocation(2, new DoorLocation(office));
            office.AddLocation(3, new CabinetLocation(office));
            office.AddLocation(4, new ArmchairLocation(office));

            Room corridor = new Room("CORRIDOR");
            corridor.AddLocation(5, new CorridorLocation(corridor));

            office.AdjacentRoom = corridor;
            corridor.AdjacentRoom = office;

            currentLocation = corridor.GetLocations()[5];

            HandleItem handleItem = (HandleItem)currentLocation.AvailableItems[0];
            handleItem.LocationToUnlock = currentLocation.Room.AdjacentRoom.GetLocations()[2];

            items.LockItem lockItem = (items.LockItem)office.GetLocations()[2].AvailableItems[1];
            lockItem.LocationToUnlock = corridor.GetLocations()[5];

            vocabulary["G"] = "GO";
            vocabulary["U"] = "USE";
            vocabulary["T"] = "TAKE";
            vocabulary["I"] = "ITEMS";
            vocabulary["D"] = "DESCRIBE";
            vocabulary["H"] = "HELP";
            vocabulary["E"] = "EXIT";

            Item phone = new items.PhoneItem();
            phone.Description = $"{phone.ItemName} - You can use it to search for information on the WEB.";
            collectedItems.Add(phone); ;
        }

        private void showCollectedItems()
        {
            Console.WriteLine("\nYour collected items are:");

            foreach (Item item in collectedItems)
            {
                Console.WriteLine(item.Description);
            }
        }

        private bool takeItem()
        {
            showAvailableItems();

            Console.Write("\nWhich one you want to take?: ");

            String input = getInput();
            Item takenItem = null;

            foreach (Item item in currentLocation.AvailableItems)
            {
                if (input == item.ItemName)
                {
                    takenItem = item;
                }
            }

            if (takenItem == null)
            {
                Console.WriteLine("\nThis item in not present here.");
                return false;
            }
            else if (!takenItem.IsCollectible)
            {
                Console.WriteLine("\nYou can't take this item");
                return false;
            }

            collectedItems.Add(takenItem);
            currentLocation.RemoveItem(takenItem);

            return true;
        }

        private bool useItem()
        {
            showCollectedItems();
            showAvailableItems();

            Console.Write("\nWhich one you want to use?: ");

            String input = getInput();
            Item foundItem = null;

            foreach (Item item in collectedItems)
            {
                if (input == item.ItemName)
                {
                    foundItem = item;
                }
            }

            foreach (Item item in currentLocation.AvailableItems)
            {
                if (input == item.ItemName)
                {
                    foundItem = item;
                }
            }

            if (foundItem == null)
            {
                Console.WriteLine("\nYou don't have this item.");
                return false;
            }
            else if (!foundItem.IsUsable)
            {
                Console.WriteLine("\nYou can't use this item.");
                return false;
            }

            if (foundItem is IUsable usableItem)
                usableItem.Use();

            return true;
        }

        private bool changeLocation()
        {
            showAvailableLocations();

            Console.Write("\nWhere do you want to go?: ");

            String input = getInput();
            string foundExit = null;
            Location foundLocation = null;

            foreach (string exit in currentLocation.Exits)
            {
                if (input == exit)
                {
                    foundExit = exit;
                }
            }

            if (foundExit == null)
            {
                Console.WriteLine("\nLocation not available or unknown.");
                return false;
            }

            foreach (Location location in currentLocation.Room.GetLocations().Values)
            {
                if (input == location.LocationName)
                {
                    foundLocation = location;
                }
            }

            foreach (Location location in currentLocation.Room.AdjacentRoom.GetLocations().Values)
            {
                if (input == location.LocationName)
                {
                    foundLocation = location;
                }
            }

            if (foundLocation.IsLocked)
            {
                Console.WriteLine("\nYou can't go there. Unlock it first.");
                return false;
            }

            currentLocation = foundLocation;

            if (foundExit == "CORRIDOR")
            {
                if (checkIfDocumentsFound())
                {
                    Console.WriteLine("You did it!");
                    Environment.Exit(0);
                }
                else
                {
                    Console.WriteLine("Did you forget about documents?");
                }
            }

            return true;
        }

        private void describeLocation()
        {
            currentLocation.Room.DescribeRoom();
            currentLocation.DescribeLocation();
        }

        private void showAvailableLocations()
        {
            List<string> exits = currentLocation.Exits;
            Console.WriteLine("\nAvailable locations:");

            foreach (string exit in exits)
            {
                Console.WriteLine(exit);
            }
        }

        private void showAvailableItems()
        {
            List<Item> items = currentLocation.AvailableItems;
            Console.WriteLine("\nAvailable items in current location:");

            foreach (Item item in items)
            {
                Console.WriteLine(item.Description);
            }
        }

        private bool checkIfDocumentsFound()
        {
            bool found = false;

            foreach (Item item in collectedItems)
            {
                if (item.ItemName == "DOCUMENTS")
                {
                    found = true;
                    break;
                }
            }

            return found;
        }

        private void showHelp()
        {
            Console.WriteLine("\nAvailable options:\n" +
                "GO(G) - go somewhere\n" +
                "USE(U) - use item\n" +
                "TAKE(T) - take item from current location\n" +
                "ITEMS(I) - list of your collected items\n" +
                "DESCRIBE(D) - description of where you are\n" +
                "HELP(H) - show available options\n" +
                "EXIT(E) - exit game");
        }

        private void exitGame()
        {
            Console.WriteLine("\nTry again later.");
        }

        public void ShowIntro()
        {
            Console.WriteLine("------------------INTRO----------------------\n\n" +
                "Welcome to Escape Office, a thrilling game where you are on a mission\n" +
                "to find secret documents " +
                "from your boss and escape the office before getting caught.\n\n" +
                "As you navigate through the various locations of the office,\n" +
                "you'll need to search for clues, solve puzzles,\n" +
                "and unlock doors to progress further.\n" +
                "However, the game doesn't have a timer or clock ticking down,\n" +
                "allowing you to take your time and fully immerse yourself\n" +
                "in the game's environment without any added pressure.\n" +
                "But don't get too comfortable - the office is full of challenges and obstacles\n" +
                "that will test your skills and abilities to think creatively and logically.\n" +
                "Can you successfully find the secret documents and make your escape before it's too late?\n" +
                "Get ready to find out in Escape Office!\n");
        }

    }
}
