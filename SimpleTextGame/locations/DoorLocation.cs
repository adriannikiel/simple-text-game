﻿namespace SimpleTextGame
{
    public class DoorLocation : Location
    {
        public DoorLocation(Room room) : base(room, "DOOR", true)
        {
        }

        protected override void addExits()
        {
            Exits.Add("DESK");
            Exits.Add("CORRIDOR");
        }

        protected override void addItems()
        {
            Item item = new Item("DOOR", false, false);
            item.Description = $"{item.ItemName} - Unfortunately there is no handle. To get out you have to unlock the electric lock";
            AvailableItems.Add(item);

            item = new items.LockItem();
            item.Description = $"{item.ItemName} - Write PIN to unlock the DOOR and exit your boss OFFICE";
            AvailableItems.Add(item);
        }

        protected override void addDescription()
        {
            Description = $"You are standing in front of an exit DOOR.";
        }
    }
}
