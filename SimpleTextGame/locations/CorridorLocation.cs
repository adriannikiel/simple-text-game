﻿namespace SimpleTextGame
{
    public class CorridorLocation : Location
    {
        public CorridorLocation(Room room) : base(room, "CORRIDOR", true)
        {
        }

        protected override void addExits()
        {
            Exits.Add("DOOR");
        }

        protected override void addItems()
        {
            HandleItem item = new HandleItem();
            item.Description = $"{item.ItemName} - You can use it to open the DOOR.";

            AddItem(item);
        }

        protected override void addDescription()
        {
            Description = "You are standing in front of your boss's DOOR.";
        }
    }
}
