﻿namespace SimpleTextGame
{
    public class CabinetLocation : Location
    {
        public CabinetLocation(Room room) : base(room, "CABINET", false)
        {
        }

        protected override void addExits()
        {
            Exits.Add("DESK");
        }

        protected override void addItems()
        {
            Item item = new Item("CABINET", false, false);
            item.Description = $"{item.ItemName} - There are lots of uninteresting books.";
            AvailableItems.Add(item);
        }

        protected override void addDescription()
        {
            Description = "You are standing in front of a CABINET.";
        }
    }
}
