﻿namespace SimpleTextGame
{
    public class ArmchairLocation : Location
    {
        public ArmchairLocation(Room room) : base(room, "ARMCHAIR", false)
        {
        }

        protected override void addExits()
        {
            Exits.Add("DESK");
        }

        protected override void addItems()
        {
            Item item = new Item("ARMCHAIR", false, false);
            item.Description = $"{item.ItemName} - You can sit on it and think about your life.";
            AvailableItems.Add(item);

            item = new items.PaintingItem(this);
            item.Description = $"{item.ItemName} - It is a big painting of your BOSS.";
            AvailableItems.Add(item);
        }

        protected override void addDescription()
        {
            Description = "You are standing in front of an ARMCHAIR.";
        }
    }
}
