﻿namespace SimpleTextGame
{
    public class WindowLocation : Location
    {
        public WindowLocation(Room room) : base(room, "WINDOW", false)
        {
        }

        protected override void addExits()
        {
            Exits.Add("DESK");
        }
        protected override void addItems()
        {
            Item item = new Item("WINDOW", false, false);
            item.Description = $"{item.ItemName} - There are no handles, so don't even try to open it.";
            AvailableItems.Add(item);

            item = new items.BlindItem(Room);
            item.Description = $"{item.ItemName} - You can use it to darken a room.";
            AvailableItems.Add(item);
        }

        protected override void addDescription()
        {
            Description = "You are standing in front of a WINDOW.";
        }

    }
}
