﻿using System;
using System.Collections.Generic;

namespace SimpleTextGame
{
    abstract public class Location
    {
        public Room Room { get; private set; }
        public string LocationName { get; private set; }
        public bool IsLocked { get; set; }
        public string Description { get; protected set; }
        public List<string> Exits { get; protected set; } = new List<string>();
        public List<Item> AvailableItems { get; protected set; } = new List<Item>();

        public Location(Room room, string name, bool isLocked)
        {
            Room = room;
            LocationName = name;
            IsLocked = isLocked;

            addExits();
            addItems();
            addDescription();
        }

        public void DescribeLocation()
        {
            Console.WriteLine(Description);
        }

        abstract protected void addExits();
        abstract protected void addItems();
        abstract protected void addDescription();

        public void AddItem(Item addedItem)
        {
            AvailableItems.Add(addedItem);
        }

        public bool RemoveItem(Item removedItem)
        {
            return AvailableItems.Remove(removedItem);
        }

        public override bool Equals(object obj)
        {
            return obj is Location other && this.LocationName == other.LocationName;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(LocationName);
        }
    }
}
