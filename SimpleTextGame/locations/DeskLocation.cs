﻿namespace SimpleTextGame
{
    public class DeskLocation : Location
    {
        public DeskLocation(Room room) : base(room, "DESK", false)
        {
        }

        protected override void addExits()
        {
            Exits.Add("WINDOW");
            Exits.Add("DOOR");
            Exits.Add("CABINET");
            Exits.Add("ARMCHAIR");
        }

        protected override void addItems()
        {
            Item item = new Item("CHAIR", false, false);
            item.Description = $"{item.ItemName} - You can sit on it, but better focus on your job.";
            AvailableItems.Add(item);

            item = new Item("COMPUTER", false, false);
            item.Description = $"{item.ItemName} - It is locked. If you had more time you might have cracked the password.";
            AvailableItems.Add(item);

            item = new Item("PICTURE", false, false);
            item.Description = $"{item.ItemName} - It is a picture of your BOSS's entire family.";
            AvailableItems.Add(item);

            item = new items.DrawerItem(this);
            item.Description = $"{item.ItemName} - You should open it. Something interesting cound be inside.";
            AvailableItems.Add(item);
        }

        protected override void addDescription()
        {
            Description = "You are standing in front of your boss's DESK.";
        }

    }
}
