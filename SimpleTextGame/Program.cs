﻿using System;

namespace SimpleTextGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game("Escape from Office");
            game.Play();
        }
    }
}
